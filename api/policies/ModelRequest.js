/**
 * ModelRequest
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */

module.exports = function (req, res, next) {
  if (req.param('user')) {
    criteria = { where: { uid: req.param('user') }}
  }

  console.log(criteria)

  if (criteria) {
    Users.findOne(criteria).populate('favourites').exec(function (err, user) {
      if (err) return cb(err)
      if (!user) return res.forbidden('You are not logged. You are not permitted to perform this action.')
      if (req.param('movie')) {
						// findOne Movie
      }
      ReqService.user(user)
      console.log('#A:- authorized user uid: ' + user.uid + ' - id: ' + user.id)
      return next()
    })
  } else {
    return next()
  }
}
