/**
 * UserController
 *
 * @description :: Server-side logic for managing users.
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
// Settings for the mail sender used for the password recovery
module.exports = {

	// register: Get all favourites
  get: function (req, res) {
    user = ReqService.user()
    return res.json({
      message: 'OK',
      favourites: user.favourites
    })
  },
	// register: Adds a new favourite to the database
  add: function (req, res) {
    user = ReqService.user()
    Favourites.findOrCreate({
      user: user.id,
      movie: req.param('movie')
    }, {
      user: user.id,
      movie: req.param('movie')
    }).exec(function (err, favourite) {
      if (err) {
        return res.json(500, {
          error: err
        })
      }
      if (!favourite) {
        return res.json(400, {
          message: 'An error occured, retry.'
        })
      }

      return res.json({
        message: 'OK',
        favourite: favourite
      })
    })
  },

  delete: function (req, res) {
    Favourites.destroy({
      movie: req.param('movie')
    }).exec(function (err) {
      return res.json({
        message: 'OK'
      })
    })
  }

}
