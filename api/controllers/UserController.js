/**
 * UserController
 *
 * @description :: Server-side logic for managing users.
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
// Settings for the mail sender used for the password recovery
module.exports = {

	// getData: Returns generic data of logged user
  create: function (req, res) {
    Users.create({
      uid: req.param('user')
    }).exec(function (err, user) {
      if (err) {
        return res.json(500, {
          error: err
        })
      }
      if (!user) {
        return res.json(400, {
          message: 'An error occured, retry.'
        })
      }

      return res.json({
        message: 'OK',
        user: user
      })
    })
  },

  get: function (req, res) {
    var u = ReqService.user()

    return res.json(200, {
      user: u
    })
  }

}
