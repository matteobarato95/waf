// Users.js

module.exports = {
  attributes: {
    id: {
      type: 'integer',
      unique: true,
      autoIncrement: true,
      primaryKey: true
    },
    uid: {
      type: 'string',
      unique: true
    },
    favourites: {
      collection: 'favourites',
      via: 'user'
    },

    toJSON: function () {
      var obj = this.toObject()
      return obj
    }
  }
}
