// Users.js

module.exports = {
  attributes: {
    id: {
      type: 'integer',
      unique: true,
      autoIncrement: true,
      primaryKey: true
    },
    movie: {
      type: 'integer',
      required: true
    },
    user: {
      model: 'users'
    },

    toJSON: function () {
      var obj = this.toObject()
      return obj
    }
  }
}
