app.controller('favouritesController',
($rootScope, $scope, $timeout, $http, $location, tmdbMovie) => {
  tmdbMovie.setup(Settings.api_key, true)
  $('#header').removeClass('alt')

  $scope.options = {
    orderBy: {
      active: false,
      value: ''
    }
  }
  var list = []

  $scope.movies = []
  $rootScope.progress_bar.animate(0.6)

  params = { language: Settings.lang, include_image_language: null }

  var c = 0
  Favorites().list().then((list) => {
    for (var i in list) {
      if (list.hasOwnProperty(i)) {
        setTimeout(() => {
          tmdbMovie.details(list.pop().movie, params, res => {
            $scope.movies.push(res)
          })
        }, c)
      }
      c += 200
    }
    $rootScope.progress_bar.animate(1.0, () => { $rootScope.progress_bar.set(0) })
  })

  $scope.removeFavorites = (id) => {
    Favorites(id).remove()
    var index = $scope.movies.findIndex((el) => {
      return el.id == id
    })
    if (index > -1) $scope.movies.splice(index, 1)
  }
})
