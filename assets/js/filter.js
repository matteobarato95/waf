var Filter = function () {
  this.genres = []
  this.keywords = []
  this.persons = []
  this.searched = []
  this.actions = { searchTag: false }

  this.count = function () { return this.genres.length + this.keywords.length + this.persons.length }

  this.serialize = function (type) {
    if (!this[type] || this[type].length <= 0) return ''
    var txt = ''
    for (i in this[type]) {
      txt += this[type][i].id + ','
    }
    txt = txt.slice(0, -1)
    return txt
  }
  this.add = function (obj, type) {
    if (this.contain(obj, type)) return false // check if already exist in filter
    if (!Settings.firts_dbclick) {
      alertify.logPosition('top left')
      alertify.delay(3000).log('<h3><i class="material-icons">touch_app</i> double click to discover the category</h3>')
      alertify.reset()
      Settings.firts_dbclick = true
    }
    if (this[type]) {
      this[type].push(obj)
      Engine.add(obj, type)
    }
  }
  this.remove = function (obj, type) {
    if (this[type]) {
      var index
      this[type].find(function (el, i) {
        if (el.id == obj.id) {
          index = i
          return true
        }
        return false
      })
      if (index > -1) {
        this[type].splice(index, 1)
        Engine.remove(obj, type, 0.5)
      }
    }
  }
  this.toggle = function (obj, type) {
    if (this.contain(obj, type)) {
      this.remove(obj, type)
    } else {
      this.add(obj, type)
    }
  }
  this.contain = function (obj, type) {
    if (this[type]) {
      var index = this[type].find(function (el, i) {
        return (el.id == obj.id)
      })
      if (index) return true
      return false
    }
  }
  this.addSearched = function (obj, type) {
    this.toggle(obj, type)
    this.emptySearched()
  }
  this.emptySearched = function () {
    this.searched = []
  }
}
