app.controller('homeController', ['$rootScope', '$scope', '$routeParams', '$route', '$timeout', '$http', '$location', '$window', 'tmdbMovie',
			($rootScope, $scope, $routeParams, $route, $timeout, $http, $location, $window, tmdbMovie) => {
  $window.scrollTo(0, 0)
  tmdbMovie.setup(Settings.api_key, true)

  $('#header').addClass('alt')

  $('#banner').scrollex({
    bottom: $('#header').outerHeight(),
    terminate: function () { $('#header').removeClass('alt') },
    enter: function () { $('#header').addClass('alt') },
    leave: function () { $('#header').removeClass('alt') }
  })

  var CURRENT_PAGE = 0
  $scope.curr_page = 0

  $scope.limit = {
    images: 9,
    persons: 4,
    default: function () {
      this.images = 9
      this.persons = 4
    }
  }
  $scope.triangle = {
    style1: '#333333',
    style2: '#6D2F2F',
    style3: '#333333'
  }

  $scope.movie = {}
  $scope.pages = [{}, {}, {}, {}]

  $scope.history = { movies: [] }

  var ID = $routeParams.movie

  $scope.submit = function () {}

  document.onkeydown = function (e) {
    e = e || window.event
    if (e.keyCode == 39) // RIGHT
					{
      $('.slick').slick('slickNext')
    } else if (e.keyCode == 37) // LEFT
					{
      $('.slick').slick('slickPrev')
    }
  }

  $scope.nextPreview = () => {
    $scope.history.movies.push($scope.movie.id)
    $('.slick').slick('slickNext')
  }
  $scope.prevPreview = () => {
    $('.slick').slick('slickPrev')
  }
  var getPrediction = (page, id) => {
    if (id) {
      tmdbMovie.details(id, params, res => {
        $scope.pages[page] = res
        if (Favorites($scope.pages[page].id).find() > -1) {
          $scope.pages[page].favourite = true
        }
        document.getElementById('page' + page).style.backgroundImage = "url('https://image.tmdb.org/t/p/w1280/" + $scope.pages[page].backdrop_path + "')"
      })
      return
    }

    var f_genres = $rootScope.filter.serialize('genres'),
      f_keywords = $rootScope.filter.serialize('keywords'),
      f_persons = $rootScope.filter.serialize('persons')
    var results = []
    var params = { with_genres: f_genres, with_keywords: f_keywords, with_people: f_persons, language: 'it-IT', page: 1 }
    tmdbMovie.discover(params, res => {
      results = results.concat(res.results)
      params.page = 2
      tmdbMovie.discover(params, res => {
        results = results.concat(res.results)
        if (results.length === 0) return false
        var notsee = notSee(results)
        console.log('get Prediction for page ' + page + ' movie :' + notsee.id)
        $scope.pages[page] = notsee
        if (Favorites($scope.pages[page].id).find() > -1) {
          $scope.pages[page].favourite = true
        }
        document.getElementById('page' + page).style.backgroundImage = "url('https://image.tmdb.org/t/p/w1280/" + $scope.pages[page].backdrop_path + "')"
      }, errorCB)
    }, errorCB)
  }
  getPrediction(0, ID)
  getPrediction(1)
  getPrediction(2)
  getPrediction(3)
  getPrediction(4)

  var getMovie = (id, movie) => {
    $scope.limit.default()
    $rootScope.progress_bar.animate(0.7) // Number from 0.0 to 1.0

    if (id) { // is id get Movie and inside Keyw Credits Images
      params = { language: Settings.lang, append_to_response: 'images,videos,keywords,credits', include_image_language: null }
      tmdbMovie.details(id, params, res => {
        if (!res.overview || res.overview.length < 5) {
          return getMovie()
        } // if not movie overview => another film

        $scope.movie = res

        $scope.pages[CURRENT_PAGE].tagline = res.tagline
        $scope.pages[CURRENT_PAGE].credits = res.credits

        $scope.persons = res.credits
        $scope.keywords = res.keywords.keywords
        $scope.images = res.images
        $scope.video = res.videos.results

        $scope.movie.favourite = $scope.pages[CURRENT_PAGE].favourite

        record(res.genres)
        record(res.keywords)
        $rootScope.progress_bar.animate(0.8)
        shadowImage('https://image.tmdb.org/t/p/w300/' + res.backdrop_path)
        $scope.movie.load_all = true
      })
    } else {
      return false
    }
    return { animate: { next: animateNexttMovie, prev: animatePrevMovie } }
  }

				// getMovie(ID)

  $scope.getKeywordMovies = (id) => {
    $rootScope.progress_bar.animate(0.3)
    tmdbMovie.discover({ with_keywords: id, sort_by: 'popularity.desc', language: +Settings.lang }, res => {
      $rootScope.progress_bar.animate(0.5)
      var notsee = notSee(res.results)
      getMovie(notsee.id).animate.next()
    }, errorCB)
  }
  $scope.getGenreMovies = (id) => {
    $rootScope.progress_bar.animate(0.3)
    tmdbMovie.discover({ with_genres: id, sort_by: 'popularity.desc', language: +Settings.lang, page: 1 }, res => {
      $rootScope.progress_bar.animate(0.5)
      notsee = notSee(res.results, 20)
      getMovie(notsee).animate.next()
    }, errorCB)
  }
  $scope.getPersonMovies = (id) => {
    $rootScope.progress_bar.animate(0.3)
    $http.get('https://api.themoviedb.org/3/person/' + id + '/movie_credits?api_key=' + Settings.api_key + '&language=' + Settings.lang).then(res => {
      $rootScope.progress_bar.animate(0.5)
      var notsee = notSee(res.data.cast)
      getMovie(notsee.id).animate.next()
    })
  }

  $scope.startVideo = () => {
    if ($scope.video.length > 0) { $window.open('https://www.youtube.com/watch?v=' + $scope.video[0].key) }
  }
  $scope.googleMovie = () => {
    $window.open('https://www.google.it/search?q=' + $scope.movie.title)
  }
  $scope.toogleFavorites = () => {
    if (!$scope.movie.favourite) {
      Favorites($scope.movie.id).add()
      $scope.movie.favourite = true
      $scope.pages[CURRENT_PAGE].favourite = true
    } else {
      Favorites($scope.movie.id).remove()
      $scope.movie.favourite = false
      $scope.pages[CURRENT_PAGE].favourite = false
    }
  }

  var shadowImage = (hsrc) => {
    var downloadingImage = new Image(300, 300)
    downloadingImage.onload = function () {
      Vibrant.from(this.src + '?' + new Date().getTime()).getPalette(function (err, palette) {
        if (err) { console.log(err) }
        console.log(palette)
        $timeout(() => {
          $('#one').css('background-color', palette.Vibrant.getHex())
	        $scope.triangle.style1 = palette.Vibrant.getHex()
	        $('#two').css('background-color', palette.Muted.getHex())
	        $scope.triangle.style2 = palette.Muted.getHex()
	        $('#three').css('background-color', palette.Vibrant.getHex())
	        $scope.triangle.style3 = palette.Vibrant.getHex()
	        $rootScope.progress_bar.animate(1.0, () => { $rootScope.progress_bar.set(0) })
        }, 200)
      })
    }
    downloadingImage.src = hsrc
    downloadingImage.crossOrigin = 'Anonymous'
  }

  var handler_scroll = function () {
    console.log($scope.movie && !$scope.movie.load_all && window.scrollY >= (screen.height * 0.26 - 10))
    if ($scope.movie && !$scope.movie.load_all && window.scrollY >= (screen.height * 0.05)) {
      window.removeEventListener('scroll', handler_scroll, false)
      getMovie($scope.pages[CURRENT_PAGE].id)
    }
  }

					// SLICK
  $timeout(() => {
    var slick = $('.slick')
    window.addEventListener('scroll', handler_scroll, false)
    slick.slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      touchThreshold: 3,
      verticalSwiping: false
    })
    slick.on('afterChange', function (event, slick, direction) {
      if (CURRENT_PAGE != slick.currentSlide) {
        CURRENT_PAGE = slick.currentSlide
        $route.updateParams({ movie: $scope.pages[CURRENT_PAGE].id })
        console.info('current Slide: ', CURRENT_PAGE)
        $scope.movie = $scope.pages[CURRENT_PAGE]
        $scope.curr_page = CURRENT_PAGE
        getPrediction((CURRENT_PAGE + 4) % slick.slideCount) // sixiest page
        $window.scrollTo(0, 0)

        window.addEventListener('scroll', handler_scroll, false)
      }
    })
  })
}
			])

		/* ----------------- */
		/* --- UTILITIES --- */
		/* ----------------- */

var notSee = (results) => {
  var MAX_MOVIE = 100
  var history = JSON.parse(localStorage.getItem('history'))
  if (!history) history = { movies: [] }
  var yet_views = history.movies

  results = results.sort(function (a, b) { // ORDINA PER popolarita
    return a.popularity - b.popularity
  })

  if (results.length == 0) return 0
  results = results.slice(0, 200) // SELEZIONA I PRIMI 200

			// FILTRA QUELLI NON VISTI

  res_length = results.length // counter for bottom function
  results = results.filter(function (a) {
    if (res_length <= 1) return true
    for (id in yet_views) {
      if (a.id == yet_views[id]) {
        res_length--
        return false
      }
    }
    return true
  })

  if (results.length == 1) yet_views = [] // if all movies in yet_views are yet views reset yet_views

  var n = Math.floor(Math.random() * (results.length)) // NE SCEGLIE UNO A CASO
  console.log('NON VIST', results.length)

  if (yet_views.length >= MAX_MOVIE) yet_views.shift()
  yet_views.push(results[n].id)
  history.movies = yet_views
  localStorage.setItem('history', JSON.stringify(history))
  return results[n]
}
var animatePrevMovie = () => {
  var banner = $('#banner>.inner')
  banner.removeClass('fadeInLeft')
  banner.removeClass('fadeIn')
  banner.removeClass('zoomIn')
  banner.addClass('zoomOut')
  banner.addClass('fadeOut')
  setTimeout(() => {
    banner.removeClass('zoomOut')
    banner.removeClass('fadeOut')
    banner.addClass('fadeInLeft')
  }, 700)
}
var animateNexttMovie = () => {
  var banner = $('#banner>.inner')
  banner.removeClass('fadeInLeft')
  banner.removeClass('fadeIn')
  banner.removeClass('zoomIn')
  banner.addClass('fadeOutLeft')
  setTimeout(() => {
    banner.removeClass('fadeOutLeft')
    banner.addClass('fadeIn')
    banner.addClass('zoomIn')
  }, 1000)
}

var errorCB = (err) => {
  alertify.log(err)
}

var record = (obj) => {
  var record = localStorage.getItem('record')
  if (record) {
    record = JSON.parse(record)
  } else {
    record = []
  }
  if (Array.isArray(obj)) {
    record = record.concat(obj)
  } else { record.push(obj) }
  localStorage.setItem('record', JSON.stringify(record))
}
