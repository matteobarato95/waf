var Settings = {
  lang: 'it-IT',
  firts_dbclick: false,
  api_key: '9d9a6c19eff90b0ee432db50c992d94d'
}
var u = (user) => {
  if (user) this.user = user
  return this.user
}
var Engine = new Engine()
Engine.restore()

var app = angular.module('waf', ['ngRoute', 'ngAnimate', 'ngTouch', 'tmdb', 'ng-mfb', 'chart.js'])

app.directive('sglclick', ['$parse', function ($parse) {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      var fn = $parse(attr['sglclick'])
      var delay = 300,
        clicks = 0,
        timer = null
      element.on('click', function (event) {
        clicks++ // count clicks
        if (clicks === 1) {
          timer = setTimeout(function () {
            scope.$apply(function () {
              fn(scope, { $event: event })
            })
            clicks = 0 // after action performed, reset counter
          }, delay)
        } else {
          clearTimeout(timer) // prevent single-click action
          clicks = 0 // after action performed, reset counter
        }
      })
    }
  }
}])

app.run(($http, $rootScope, $location) => {
  setTimeout(() => { $('#loading').addClass('hide') }, 3200)
  setTimeout(() => { $('#loading').css('display', 'none') }, 4500)

  $rootScope.filter = new Filter()

  $rootScope.searchTags = (pseudo_tag) => {
    $rootScope.filter.emptySearched()
    $http.get('https://api.themoviedb.org/3/search/keyword?api_key=' + Settings.api_key + '&query=' + pseudo_tag).then(res => {
      console.log('key ser', res.data)
      for (i = 0; i < 3 && i < res.data.results.length; i++) {
        res.data.results[i].type = 'keywords'
        $rootScope.filter.searched.push(res.data.results[i])
      }
      $http.get('https://api.themoviedb.org/3/search/person?api_key=' + Settings.api_key + '&query=' + pseudo_tag).then(res => {
        console.log('perospn se', res.data)
        for (i = 0; i < 2 && i < res.data.results.length; i++) {
          res.data.results[i].type = 'persons'
          $rootScope.filter.searched.push(res.data.results[i])
        }
      })
    })
  }

  angular.element(document).ready(function () {
    mainJS(jQuery)
    $rootScope.user = {}

    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
				// User is signed in.
        $rootScope.user = user
        u(user)
        user.getToken().then(function (accessToken) {
          document.getElementById('sign-in-status').textContent = 'Signed in'
          document.getElementById('sign-in').textContent = 'Sign out'
          document.getElementById('account-details').textContent = JSON.stringify({
            displayName: displayName,
            email: email,
            emailVerified: emailVerified,
            photoURL: photoURL,
            uid: uid,
            accessToken: accessToken,
            providerData: providerData
          }, null, '  ')
        })
      }
    }, function (error) {
      console.log(error)
    })
  })

  $rootScope.progress_bar = new ProgressBar.Line('#progress-bar', {
    strokeWidth: 1,
    easing: 'easeInOut',
    duration: 1400,
    color: '#E53935',
    from: {color: '#1E88E5'},
    to: {color: '#E53935'},
    trailColor: 'rbga{255,255,255,0}',
    trailWidth: 1,
    svgStyle: { width: '100%', height: '100%' },
    step: (state, bar) => {
      bar.path.setAttribute('stroke', state.color)
    }
  })
})
app.factory('HttpInterceptorMessage', ['$q', '$location', '$rootScope', function ($q, $location, $rootScope) {
  return {
		// optional method
    'request': function (config) {
      return config
    },
    'response': function (response) {
			// do something on success
      if (response.data.message) {
        alertify.success(response.data.message)
      };
      return response
    },

    'responseError': function (response) {
			// do something on error

      if (response.data && response.data.message) {
        alertify.error(response.data.message)
      }
      if (response.data && response.data.error) {
        alertify.error(response.data.error.error || response.data.error)
      }

      return $q.reject(response)
    }
  }
}])

app.config(['$locationProvider', '$routeProvider', '$httpProvider',
	function ($locationProvider, $routeProvider, $httpProvider) {
  $routeProvider
			.when('/', {
  templateUrl: 'js/home/home.template.html',
  reloadOnSearch: false
})
			.when('/login', {
  templateUrl: 'js/login/login_firebase.template.html'
})
			.when('/chart', {
  templateUrl: 'js/chart/chart.template.html'
})
			.when('/favorites', {
  templateUrl: 'js/favourites/favourites.template.html'
})

		// TODO: $locationProvider.html5Mode(true)
  $httpProvider.interceptors.push('HttpInterceptorMessage')
}
])
