var URL = 'http://cipizio.tk:1337'

var Engine = function () {
  this.tags = []
  this.add = function (obj, type, points) {
    if (!points) points = 1 // defualt points

    var find = this.tags.findIndex((el) => {
      return (el.id == obj.id && el.type == type)
    })
    if (find > -1) {
      this.tags[find].points += points
    } else {
      this.tags.push({
        id: obj.id,
        name: obj.name,
        type: type,
        points: 1
      })
    }
    console.info('ENGINE: ', this.tags)
    this.save()
  }
  this.remove = function (obj, type, points) {
    if (!points) points = 0.5 // defualt points

    var find = this.tags.findIndex((el) => {
      return (el.id == obj.id && el.type == type)
    })
    if (find > -1) {
      this.tags[find].points -= points
      if (this.tags[find].points < 0) {
        this.tags.splice(find, 1)
      }
    }
    this.save()
    console.info('ENGINE: ', this.tags)
  }
  this.upvoted = function (max) {
    var list = this.tags
    list = list.sort((a, b) => {
      return a.points - b.points
    })
    return list.slice(0, max)
  }
  this.getWeightPoint = function (points, type) {
    if (!points) return 0 // default points
    return points // lenght filter
  }

  this.restore = function () {
    var data = localStorage.getItem('Engine')
    if (data) {
      var engine = JSON.parse(data)
      console.info('| ENGINE RESTORED| ', engine, '\n|-tags length:  ', engine.tags ? engine.tags.length : 0, ' \n')
      if (engine.tags && engine.tags.length > 0) this.tags = engine.tags
    }
  }
  this.save = function () {
    var data = JSON.stringify(this)
    return localStorage.setItem('Engine', data)
  }
  this.debug = function () {
    return JSON.stringify(this.tags)
  }
}

// ________________________________
// ________________________________
// ________________________________

var Favorites = function (id) {
  var $http = angular.injector(['ng']).get('$http')

  var FAVS
  this.id = id
  this.find = function (id) {
    if (!id) var id = this.id
    if (!FAVS) {
      FAVS = JSON.parse(localStorage.getItem('favourites'))
      if (!FAVS || FAVS.length < 1) return -1
    }

    return (FAVS.findIndex((el) => {
      return el.movie == id
    }))
  }

  this.add = function () {
    return new Promise(
			function (resolve, reject) {
  $http.get(URL + '/user/' + u().uid + '/favourite/movie/' + (this.id) + '/add').then((res) => { this.list(); resolve(res.data.favourite) })
})
  }

  this.remove = function () {
    return new Promise(
			function (resolve, reject) {
  $http.get(URL + '/user/' + u().uid + '/favourite/movie/' + (this.id) + '/delete').then((res) => { this.list(); resolve(res.data.favourite) })
})
  }

  this.list = function () {
    return new Promise(
			function (resolve, reject) {
  $http.get(URL + '/user/' + u().uid + '/favourites').then((res) => {
    localStorage.setItem('favourites', JSON.stringify(res.data.favourites))
    FAVS = res.data.favourites
    resolve(res.data.favourites)
  })
  if (!FAVS) {
    FAVS = JSON.parse(localStorage.getItem('favourites'))
    return FAVS
  }
})
  }

  return this
}
